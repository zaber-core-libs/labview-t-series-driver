# Zaber T-Series LabVIEW Driver

This is the version control repository for Zaber's official T-Series
(Binary protocol) LabVIEW library. This library works with T-Series Zaber 
devices and with A-Series and X-Series devices if they are configured
to use the Binary protocol.

The official install source for this library is via the 
[National Instruments Instrument Driver Network](http://sine.ni.com/apps/utf8/niid_web_display.download_page?p_id_guid=63377C8949896328E04400144F1EF859). 
Installing via the IDN gets you the most recent version that has been
certified by National Instruments.

Documentation is available on the [Zaber website](https://www.zaber.com/wiki/Software/Binary_Labview_Driver).

